function editCheck() {

  const login_id = document.getElementById('login_id').value;
  const login1_error_message = document.getElementById("login1-error-message")
  const login2_error_message = document.getElementById("login2-error-message")
  if (login_id.length < 6 || 20 < login_id.length) {
    login1_error_message.style.display = "inline";
    login2_error_message.style.display = "none";
    return false;
  } else if (login_id == "") {
    login1_error_message.style.display = "inline";
    login2_error_message.style.display = "none";
    return false;
  } else if (!login_id.match(/^[a-zA-Z0-9]{6,20}/)) {
    login2_error_message.style.display = "inline";
    login1_error_message.style.display = "none";
    return false;
  } else {
    login1_error_message.style.display = "none";
    login2_error_message.style.display = "none";
  }

  const name = document.getElementById('name').value;
  const name1_error_message = document.getElementById("name1-error-message")
  const name2_error_message = document.getElementById("name2-error-message")
  if (name.length >= 10) {
    name1_error_message.style.display = "inline";
    name2_error_message.style.display = "none";
    return false;
  } else if (name == "") {
    name2_error_message.style.display = "inline";
    name1_error_message.style.display = "none";
    return false;
  } else {
    name1_error_message.style.display = "none";
    name2_error_message.style.display = "none";
  }

  const password = document.getElementById('password').value;
  const password1_error_message = document.getElementById("password1-error-message")
  const password2_error_message = document.getElementById("password2-error-message")
  if (password.length < 6 || 20 < password.length) {
    password1_error_message.style.display = "inline";
    password2_error_message.style.display = "none";
    return false;
  } else if (password == "") {
    password1_error_message.style.display = "inline";
    password2_error_message.style.display = "none";
    return false;
  } else if (!password.match(/^[a-zA-Z0-9]{6,20}/)) {
    password2_error_message.style.display = "inline";
    password1_error_message.style.display = "none";
    return false;
  } else {
    password1_error_message.style.display = "none";
    password2_error_message.style.display = "none";
  }

  const repassword = document.getElementById('repassword').value;
  const repassword1_error_message = document.getElementById("repassword1-error-message")
  const repassword2_error_message = document.getElementById("repassword2-error-message")
  if (repassword.length < 6 || 20 < repassword.length) {
    repassword1_error_message.style.display = "inline";
    repassword2_error_message.style.display = "none";
    return false;
  } else if (repassword == "") {
    repassword1_error_message.style.display = "inline";
    repassword2_error_message.style.display = "none";
    return false;
  } else if (!repassword.match(/^[a-zA-Z0-9]{6,20}/)) {
    repassword2_error_message.style.display = "inline";
    repassword1_error_message.style.display = "none";
    return false;
  } else {
    repassword1_error_message.style.display = "none";
    repassword2_error_message.style.display = "none";
  }

  const password3_error_message = document.getElementById("password3-error-message")
  if (password != repassword) {
    password3_error_message.style.display = "inline";
    return false;
  } else {
    password3_error_message.style.display = "none";
  }
}