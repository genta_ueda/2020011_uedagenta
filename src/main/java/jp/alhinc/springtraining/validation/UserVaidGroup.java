package jp.alhinc.springtraining.validation;

import javax.validation.GroupSequence;

@GroupSequence({UserVaidGroup1.class, UserVaidGroup2.class, UserVaidGroup3.class})
public interface UserVaidGroup {

}
