package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.DepartmentPosition;
import jp.alhinc.springtraining.mapper.DepartmentPositionMapper;

@Service
public class DepartmentPositionService {
	@Autowired
	private DepartmentPositionMapper mapper;

	@Transactional
	public List<DepartmentPosition> getDepartmentPosition(){
		return mapper.getDepartmentPosition();
	}
}

