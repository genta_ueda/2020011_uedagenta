package jp.alhinc.springtraining.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class EditUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public EditUserForm edit(Long id) {
		User getUser = mapper.edit(id);
		EditUserForm form = new EditUserForm();
		BeanUtils.copyProperties(getUser, form);
		return form;
	}

	@Transactional
	public int updateEdit(EditUserForm form) {

		User entity = new User();

		entity.setLogin_id(form.getLogin_id());

		entity.setId(form.getId());

		entity.setName(form.getName());

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getPassword()));

		entity.setBranch_id(form.getBranch_id());

		entity.setDepartment_position_id(form.getDepartment_position_id());

		int edit = mapper.updateEdit(entity);
		return edit;
	}

	public boolean confirmation (Long id, String login_id) {
		User alreadyLoginId = mapper.getLoginId(login_id);
		User sameLoginId = mapper.getAlreayLoginId(id);
		User newLoginId = new User();
		newLoginId.setLogin_id(login_id);
		newLoginId.setId(id);

		if (alreadyLoginId == null) {
			return false;
		} else if  ( !(sameLoginId.getId().equals(alreadyLoginId.getId())) && alreadyLoginId.getLogin_id().equals(newLoginId.getLogin_id())) {
			return true;
		}else
			return false;
	}


}
