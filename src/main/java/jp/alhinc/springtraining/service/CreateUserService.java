package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public int create(CreateUserForm form) {
		User entity = new User();

		entity.setLogin_id(form.getLogin_id());

		entity.setName(form.getName());

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));

		entity.setBranch_id(form.getBranch_id());

		entity.setDepartment_position_id(form.getDepartment_position_id());

		return mapper.create(entity);
	}

	public boolean confirmation (String login_id) {
		User alreadyLoginId = mapper.getLoginId(login_id);
		User newLoginId = new User();
		newLoginId.setLogin_id(login_id);

		if (alreadyLoginId == null) {
			return false;
		} else if ( alreadyLoginId.getLogin_id().equals(newLoginId.getLogin_id())) {
			return true;
		}
		return false;
	}
}
