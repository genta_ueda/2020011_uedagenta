package jp.alhinc.springtraining.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.DepartmentPosition;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.form.UpdateUserForm;
import jp.alhinc.springtraining.service.BranchService;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.DepartmentPositionService;
import jp.alhinc.springtraining.service.EditUserService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.validation.UserVaidGroup;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private EditUserService editUserService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentPositionService departmentPositionService;

	public static final String STR_BRANCHES = "branches";
	public static final String STR_DEPARTMENT_POSITION = "departmentsPositions";
	public static final String USERS = "users";
	public static final String FORM = "form";
	public static final String MESSAGE = "message";

	@GetMapping
	public String index(Model model) {
		//ホーム画面に登録しているユーザーを表示
		List<User> users = getAllUsersService.getAllUsers();
		model.addAttribute(USERS, users);
		System.out.println(users);
		return "users/index";
	}

	@GetMapping("/create")
	public String create(Model model) {
		//新規登録画面でユーザーを登録する
		List<Branch> branches = branchService.getBranch();
		List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition();
		model.addAttribute(FORM, new CreateUserForm());
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENT_POSITION, departmentsPositions);
		return "users/create";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		//登録しているユーザーを編集する
		EditUserForm form = editUserService.edit(id);
		List<Branch> branches = branchService.getBranch();
		List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition();
		model.addAttribute(FORM, form);
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENT_POSITION, departmentsPositions);
		return "users/edit";
	}

	@PostMapping("/stop")
	public String stop(@Validated UpdateUserForm form, Model model) {
		//停止・復活機能
		getAllUsersService.stop(form);
		return "redirect:/users";
	}

	@PostMapping
	public String create(@ModelAttribute(FORM) @Validated(UserVaidGroup.class) CreateUserForm form, BindingResult result, Model model) {
		//バリデーションを出す処理
		List<Branch> branches = branchService.getBranch(); //支店情報取得
		List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition(); //部署・役職情報取得
		if (result.hasErrors()) {
			model.addAttribute(MESSAGE, "以下の内容を確認してください");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENT_POSITION, departmentsPositions);
			return "users/create";
		}
		boolean duplicateConfirmation = createUserService.confirmation(form.getLogin_id());
		if ( duplicateConfirmation == true ) {
			model.addAttribute(MESSAGE, "「ログインIDは既に存在します」");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENT_POSITION, departmentsPositions);
			return "users/create";
		} else {
			//formの情報をサービスへ
			createUserService.create(form);
			//ホーム画面に遷移
			return "redirect:/users";
		}
	}

	@PostMapping("/edit")
	public String edit(@ModelAttribute(FORM) @Validated(UserVaidGroup.class) EditUserForm form, BindingResult result, Model model) {
		//部署・役職情報取得
		List<Branch> branches = branchService.getBranch(); //支店情報取得
		List<DepartmentPosition> departmentsPositions = departmentPositionService.getDepartmentPosition();
		//バリデーションを出す処理
		if (result.hasErrors()) {
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENT_POSITION, departmentsPositions);
			return "users/edit";
		}
		boolean duplicateConfirmation = editUserService.confirmation(form.getId(), form.getLogin_id());
		if ( duplicateConfirmation == true ) {
			model.addAttribute(MESSAGE, "「ログインIDは既に存在します」");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENT_POSITION, departmentsPositions);
			return "users/edit";
		} else {
			//formの情報をサービスへ
			editUserService.updateEdit(form);
			//ホーム画面に遷移
			return "redirect:/users";
		}
	}
}
