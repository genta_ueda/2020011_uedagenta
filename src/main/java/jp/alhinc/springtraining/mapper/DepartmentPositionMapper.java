package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.DepartmentPosition;

@Mapper
public interface DepartmentPositionMapper {
	List<DepartmentPosition> getDepartmentPosition();
}