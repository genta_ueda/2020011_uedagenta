package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.User;

@Mapper
public interface UserMapper {

	List<User> findAll();

	int create(User entity);

	User edit(Long id);

	int updateEdit(User entity);

	int stop(User entity);

	User getLoginId(String login_id);

	User getAlreayLoginId(Long id);

}
