package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.alhinc.springtraining.validation.UserVaidGroup1;
import jp.alhinc.springtraining.validation.UserVaidGroup2;
import lombok.Data;

@Data
public class CreateUserForm implements Serializable {

	public Long id;

	@NotBlank(groups = {UserVaidGroup1.class}, message="必須項目です")
	@Pattern(groups = {UserVaidGroup2.class}, regexp = "^[a-zA-Z0-9]{6,20}", message="6文字から20文字で入力して下さい,半角英数字のみ有効です")
	public String login_id;

	@NotBlank(groups = {UserVaidGroup1.class}, message="名前を入力してください")
	@Size(groups = {UserVaidGroup2.class},max=10, message="{max}文字以下で入力して下さい")
	public String name;

	@NotBlank(groups = {UserVaidGroup1.class}, message="PWを入力してください")
	@Pattern(groups = {UserVaidGroup2.class}, regexp = "^[a-zA-Z0-9]{6,20}",message="6文字から20文字で入力して下さい半角英数字のみ有効です")
	public String rawPassword;

	@NotBlank(groups = {UserVaidGroup1.class}, message="確認用パスワードを入力してください")
	public String RePassword;

	@AssertTrue(groups = {UserVaidGroup1.class}, message="パスワードと確認用パスワードが一致しません")
	public boolean isPasswordValid() {
		if(rawPassword == null || rawPassword.isEmpty())
			return true;
		return rawPassword.equals(RePassword);
	}

	public int branch_id;

	public int department_position_id;

}
