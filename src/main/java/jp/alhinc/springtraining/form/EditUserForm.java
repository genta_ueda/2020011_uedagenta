package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.alhinc.springtraining.validation.UserVaidGroup1;
import jp.alhinc.springtraining.validation.UserVaidGroup2;
import jp.alhinc.springtraining.validation.UserVaidGroup3;
import lombok.Data;

@Data
public class EditUserForm implements Serializable {

	public Long id;

	@NotEmpty(groups = {UserVaidGroup1.class}, message = "ログインIDは必須です")
	@Size(min = 6, max = 20, groups = {UserVaidGroup2.class}, message = "ログインIDは{min}文字以上{max}文字以下で入力してください")
	@Pattern(regexp = "[a-zA-Z0-9]*", groups = {UserVaidGroup3.class}, message = "ログインIDは半角英数字で入力してください")
	public String login_id;

	@NotEmpty(groups = {UserVaidGroup1.class}, message = "名前は必須です")
	@Size(min = 1, max = 10, groups = {UserVaidGroup2.class}, message = "名前は1文字以上10文字以下で入力してください" )
	public String name;

	public String password;

	@AssertTrue(groups = {UserVaidGroup1.class}, message="記号を含む全ての半角英数字で6文字から20文字で入力して下さい")
	public boolean isRegistrationPasswordValid() {
		if((password == null || password.isEmpty()) && (RePassword == null || RePassword.isEmpty()) ||
				(password.matches("^[a-zA-Z0-9 -~]*$") && password.length() >= 6 && password.length() <= 20))
			return true;
		return false;
	}

	public String RePassword;

	@AssertTrue(groups = {UserVaidGroup1.class}, message="パスワードと確認用パスワードが一致しません")
	public boolean isPasswordValid() {
		if(password == null || password.isEmpty())
			return true;
		return password.equals(RePassword);
	}
	public int branch_id;

	public int department_position_id;


}