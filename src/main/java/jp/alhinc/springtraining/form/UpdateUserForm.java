package jp.alhinc.springtraining.form;

import java.io.Serializable;

import lombok.Data;

@Data
public class UpdateUserForm implements Serializable {

	public Long id;

	public int is_delete;

}

