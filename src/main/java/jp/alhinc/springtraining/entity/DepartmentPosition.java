package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class DepartmentPosition {

	private int id;

	private String department_position_name;

}
