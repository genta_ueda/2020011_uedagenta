package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class User {

	private Long id;

	private String login_id;

	private String name;

	private String password;

	private int branch_id;

	private String branch_name;

	private int department_position_id;

	private String department_position_name;

	private int is_delete;

}
